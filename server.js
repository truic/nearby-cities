var express = require('express')
var app = express();
var bodyParser = require('body-parser');
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json()); // support json encoded bodies
const nearbyCities = require("nearby-cities");

app.post('/get_nearby_cities', function(req, res)
{
    res.send(nearbyCities({latitude: req.body.latitude, longitude: req.body.longitude}).slice(0, req.body.count))
})

const PORT = 8080
const HOST = '0.0.0.0'

app.listen(PORT, HOST)
console.log(`Running on http://${HOST}:${PORT}`)

// var server = app.listen(8081, function()
// {
//     var host = server.address().address
//     var port = server.address().port
//     console.log("App listening at http://localhost:%s", port)
// })