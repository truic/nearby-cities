# Nearby Cities REST API
## Installation

1. open a terminal
1. enter the project dir
1. execute this command to install all required packages
    ```
    npm install
    ```
---
## Executing server.js locally

### Terminal 
1. open a terminal 
1. enter the project dir
1. execute this command to run the REST api script
    ```
    node server.js
    ```
1. the REST API server will run locally on the port `8080`

### Debug Mode in VScode
    
1. open the project dir in vscode
1. open the server.js file
1. hit `f5`
1. select Node.js from the dropdown menu
1. the REST api should execute and messages and error logs should appear in the `Debug Console` in the integrated terminal

---
## Building and running in Docker

1. open a terminal 
1. enter the project dir
1. Build the image:
    ```
    docker build --no-cache -t nearby-cities:latest .
    ```
1. Run the docker image:
    ```
    docker run --rm -p 8080:8080 --name nearby-cities nearby-cities:latest
    ```

---
## Sending requests

When the server is running, cities can be queried by sending a POST request to the url: 
```
http://{host}:{port}/get_nearby_cities
```
While running locally the url (by default will be):
```
http://localhost:8080/get_nearby_cities
```

### POST request schema

3 values are expected by the api in JSON format:

1. Latitude : float - Latitude coordinate
2. Longitude: float - Longitude coordinate
3. count: int  - the number of cities that should be returned

JSON example:

{
    "latitude": 34.4362755, 
    "longitude": -119.705086, 
    "count": 15
}
