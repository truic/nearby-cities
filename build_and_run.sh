#!/usr/bin/env bash
BASEDIR=$(dirname "$0")
docker build --no-cache -t nearby-cities:latest $BASEDIR;
docker run --rm -p 8080:8080 --name nearby-cities nearby-cities:latest;